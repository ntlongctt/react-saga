import  {
  GET_CONTACT_DETAIL, 
  GET_LIST_CONTACT,
  GET_CONTACT_DETAIL_SUCCESS, 
  GET_LIST_CONTACT_SUCCESS, 
  ADD_CONTACT, 
  ADD_CONTACT_SUCCESS,
  SAVE_CONTACT,
  SAVE_CONTACT_SUCCESS,
  DELETE_CONTACT,
  DELETE_CONTACT_SUCCESS
} from './constants';

export function getListContact() {
  return {
    type: GET_LIST_CONTACT
  }
}
export function getListContactSuccess(items) {
  return {
    type: GET_LIST_CONTACT_SUCCESS,
    items
  }
}

export function getContactDetail(id) {
  return {
    type: GET_CONTACT_DETAIL,
    id
  }
}

export function getContactDetailSuccess(item) {
  return {
    type: GET_CONTACT_DETAIL_SUCCESS,
    item
  }
}

export function addContact(contact, cb) {
  return {
    type: ADD_CONTACT,
    contact,
    cb
  }
}
export function addContactSuccess(contact) {
  return {
    type: ADD_CONTACT_SUCCESS,
    contact
  }
}

export function saveContact(contact, cb) {
  return {
    type: SAVE_CONTACT,
    contact,
    cb
  }
}
export function saveContactSuccess(contact) {
  return {
    type: SAVE_CONTACT_SUCCESS,
    contact
  }
}

export function deleteContact(contactId) {
  return {
    type: DELETE_CONTACT,
    contactId,
  }
}
export function deleteContactSuccess(contactId) {
  return {
    type: DELETE_CONTACT_SUCCESS,
    contactId
  }
}