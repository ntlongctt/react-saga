import React from "react";
import {
  View,
  Text,
  InteractionManager,
  StyleSheet,
  TextInput,
  Image,
  Alert,
  TouchableOpacity
} from "react-native";
import { Button, Avatar , Input, Icon} from 'react-native-elements';
import { withFormik } from "formik";
import * as yup from "yup";
import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker'
import DateTimePicker from 'react-native-modal-datetime-picker';
import { connect } from 'react-redux';
import { addContact, saveContact } from './actions';
import { convertToDate } from '../../utils/time-utils';

const options = {
  title: 'Select Avatar',
  customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

const styles = {
  container: {
    marginTop: 48,
    // flexDirection: 'column', 
    // alignItems: 'flex-start'
  },
  avatar: {
    alignSelf: 'center'
  },
  green : {
    backgroundColor: '#32B76C',
    height: 50,
    width: 60,
  },
  datePicker: {
    margin: 5,
    marginTop: 15
  }
}

const enhancer = withFormik({
  mapPropsToValues: props => {
    return {
      email: "",
      name: "",
      birthday: '',
      avatarUrl: '',
      filePath: '',
      id: ''
    };
  },
  validationSchema: yup.object().shape({
    // email: yup.string()
    //   .email()
    //   .required()
    name: yup.string().required(),
    avatarUrl: yup.string().required().default('https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg')
  }),

  handleSubmit: (values, { props, setSubmitting }) => {
    console.log(values);
    props.saveContact(values, () => {
      props.navigation.goBack();
    });
  }
});


class ContactDetail extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return {
      headerStyle: { backgroundColor: "#ace" },
      headerRight:
        navigation.state.params && navigation.state.params.headerRight
    };
  };

  state = {
    avatarSource: '',
    avatarUri: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    date:'',
    isDateTimePickerVisible: false,
    selectedContact: null
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.props.navigation.setParams({
        headerRight: <Button type='clear' onPress={this.props.handleSubmit} title="Save" />
      });
    });

    // setTimeout(() => {
    //   this.props.setFieldValue('email', 'test@gmail.com');
    // }, 1);

    const contactId = this.props.navigation.getParam('contactId', '');
    if (contactId) {
      const contact = this.props.contacts.items.find(i => i.id === contactId);
      setTimeout(() => {
        this.bindFormValues(contact);
      }, 1);
    }

  }

  bindFormValues = (values) => {
    this.props.setFieldValue('name', values.name);
    this.props.setFieldValue('avatarUrl', values.avatarUrl);
    this.props.setFieldValue('birthday', values.birthday);
    if (values.id) {
      this.props.setFieldValue('id', values.id);
    }
  }

  selectAvatar = () => {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
      // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        this.props.setFieldValue('avatarUrl', response.uri);
        this.props.setFieldValue('filePath', response.path);
      }
    });
  }

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = (date) => {
    // this.setState({birthday: date.toISOString()});
    this.props.setFieldValue('birthday', date.toISOString());
    this._hideDateTimePicker();
  };

  render() {
    const {
      values,
      touched,
      errors,
      handleChange,
      handleBlur,
      handleSubmit,
    } = this.props;

    return (
      <View style={styles.container}>
        <View style={styles.avatar}>
          <Avatar
            rounded
            size='xlarge'
            source={{ uri: this.props.values.avatarUrl, }}
            showEditButton
            editButton={{
              name: 'edit',
              type: 'material',
              size: 30
            }}
            onEditPress={this.selectAvatar}
          />
        </View>
        
        <View>
          <Input
            placeholder='Name'
            value={this.props.values.name}
            leftIcon={{ type: 'material', name: 'edit' }}
            onChangeText={text => this.props.setFieldValue("name", text)}
            errorMessage={touched.name && errors.name ? errors.name: ''}
          />
        </View>

        <View>
          <TouchableOpacity onPress={this._showDateTimePicker}>
            <Input
              placeholder='Birthday'
              value={convertToDate(this.props.values.birthday)}
              leftIcon={{ type: 'material', name: 'edit' }}
              onChangeText={text => this.props.setFieldValue("email", text)}
              errorMessage={touched.email && errors.email ? errors.email: ''}
              editable={false}
            />
          </TouchableOpacity>
          <DateTimePicker
            isVisible={this.state.isDateTimePickerVisible}
            onConfirm={this._handleDatePicked}
            onCancel={this._hideDateTimePicker}
          />
        </View>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    contacts: state.contacts
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
      addContact: (contact, cb) => dispatch(addContact(contact, cb)),
      saveContact: (contact, cb) => dispatch(saveContact(contact, cb)),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(enhancer(ContactDetail));