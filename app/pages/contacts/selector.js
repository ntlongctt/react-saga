import { createSelector } from 'reselect'
import { initialState } from './reducer';

const selectContacts = state => state.get('contacts', initialState);

const makeSelectContactList = () => createSelector(selectContacts, contactsState => contactsState.get('items'));
const makeSelectContactDetail = () => createSelector(selectContacts, state => state.get('selected'));

export {selectContacts, makeSelectContactList, makeSelectContactDetail};
