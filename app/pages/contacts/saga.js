import { select, takeLatest, put, takeEvery, take, call } from 'redux-saga/effects'

import { makeSelectContactList, makeSelectContactDetail } from './selector';
import { getListContactSuccess, addContactSuccess, saveContactSuccess, deleteContactSuccess } from './actions';
import { GET_LIST_CONTACT, ADD_CONTACT, SAVE_CONTACT, DELETE_CONTACT } from './constants';
import firebase from 'react-native-firebase';

function* uploadAvatar(filePath) {
  let url = '';
  yield firebase.storage().ref(`profile-images-upload_${new Date().getTime()}`).putFile(filePath).then(resp => {
    console.log(resp);
    url = resp.downloadURL;
  }).catch(err => {
    console.log(err);
  })
  return url;
}

export function* getContactList() {
  const ref = firebase.firestore().collection('contacts');
  const rs = [];
  yield ref.get().then(data => {
    data.docs.forEach(doc => {
      rs.push({
        id: doc.id,
        ...doc.data(),
      })
    }) 
  }).catch(err => {
    console.log(err);
    
  })
 
  yield put(getListContactSuccess(rs));
}

export function* addContact({contact, cb}) {
  const ref = firebase.firestore().collection('contacts');
  const url = yield uploadAvatar(contact.avatarUrl);
  const postObj = {
    name: contact.name,
    avatarUrl: url
  }
  const resp = yield ref.add(postObj);
  yield put(addContactSuccess({
    id: resp.id,
    name: contact.name,
    avatarUrl: url
  }));

  cb();
}



export function* saveContact({contact, cb}) {
  const ref = firebase.firestore().collection('contacts');
  const url = contact.filePath ? yield uploadAvatar(contact.filePath) : contact.avatarUrl;
  const id = contact.id;
  delete contact.id;
  const postObj = {...contact, avatarUrl: url};

  if (id) {
    yield ref.doc(id).update(contact);
    yield put(saveContactSuccess({
      id: id,
      avatarUrl: url,
      ...contact
    }));
  } else {
    const resp = yield ref.add(postObj);
    if (resp.id) {
      yield put(saveContactSuccess({
        ...contact,
        id: resp.id,
        avatarUrl: url,
      }));
    }
    
  }
  cb();
}

export function* deleteContact({contactId}) {
  const ref = firebase.firestore().collection('contacts');
  const resp = yield ref.doc(contactId).delete();
  yield put(deleteContactSuccess(contactId))
}

export default function* Contacts() {
  yield takeLatest(GET_LIST_CONTACT, getContactList);
  yield takeLatest(ADD_CONTACT, addContact);
  yield takeLatest(SAVE_CONTACT, saveContact);
  yield takeLatest(DELETE_CONTACT, deleteContact);
}