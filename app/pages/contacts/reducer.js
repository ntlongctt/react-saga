import {
  GET_LIST_CONTACT_SUCCESS,
  GET_CONTACT_DETAIL_SUCCESS,
  GET_LIST_CONTACT,
  ADD_CONTACT_SUCCESS,
  SAVE_CONTACT_SUCCESS,
  DELETE_CONTACT_SUCCESS
} from './constants';
import { identifier } from '@babel/types';
import { saveContact } from './saga';

export const initialState = {
  items: [],
  selected: null,

}

const saveContactToStore = (state, contact) => {
  const exitsContact = state.items.find(i => i.id === contact.id);
  if (exitsContact) {
    return {
      ...state,
      items: state.items.map(i => {
        if (i.id === contact.id) {
          return contact;
        }
        return i
      })
    }
  } else {
    return {
      ...state,
      items: [...state.items, contact]
    }
  }
}

const deleteContactFromStore = (state, contactId) => {
  return {
    ...state,
    items: state.items.filter(i => i.id !== contactId)
  }
}

export default function contactReducer(state = initialState, action) {
  switch (action.type) {
    case GET_LIST_CONTACT_SUCCESS:
      return {
        ...state,
        items: action.items
      }
    case GET_CONTACT_DETAIL_SUCCESS:
      return {
        ...state,
        selected: action.item
      }
    case ADD_CONTACT_SUCCESS:
      return {
        ...state,
        items: [...state.items, action.contact]
      }
    case SAVE_CONTACT_SUCCESS:
      return saveContactToStore(state, action.contact)
  
    case DELETE_CONTACT_SUCCESS:
      return deleteContactFromStore(state, action.contactId)
  
    default:
      return state;
  }
}