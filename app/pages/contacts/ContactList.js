import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import firebase from 'react-native-firebase';

import { 
  Text, Button,
  ListItem,
  Icon
} from 'react-native-elements';
import {
  AsyncStorage,
  InteractionManager,
  View,
  ScrollView,
  Alert
} from 'react-native';
import { getListContact, deleteContact } from './actions';
import ContactDetail from './ContactDetail';
import { TouchableOpacity } from 'react-native-gesture-handler';

const styles = {
  addContact: {
    display: 'flex',
    flexDirection: 'row',
    // justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 20,
    marginVertical: 20
  },
  textAdd: {
    fontSize: 20,
    marginLeft: 20
  }
}

const list = [
  {
    name: 'Amy Farha',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: 'Vice President'
  },
  {
    name: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    name: 'Amy Farha',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: 'Vice President'
  },
  {
    name: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    name: 'Amy Farha',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: 'Vice President'
  },
  {
    name: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
]
class ContactList extends Component {

  // static navigationOptions = {
  //   drawerLabel: 'Home',
  //   drawerIcon: ({ tintColor }) => (
  //     <Icon name="home"></Icon>
  //   ),
  // };

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Contact list',
      headerStyle: { backgroundColor: "#ace" },
      headerRight:
        navigation.state.params && navigation.state.params.headerRight
    };
  };
  // static navigationOptions = {
  //   title: 'Contact list',
  //   headerRight: navigation.state.params && navigation.state.params.headerRight
  // };

  state = {
    modalVisible: false
  }

  handleSignOut = () => {
    AsyncStorage.clear();
    this.props.navigation.navigate('Auth');
  }

  getContacts = () => {
    this.props.getListContact();
  }

  componentWillMount() {
    this.getContacts();
    InteractionManager.runAfterInteractions(() => {
      this.props.navigation.setParams({
        headerRight: <Button type='clear' onPress={this.handleSignOut} title="Logout" />
      });
    });
  }

  componentWillReceiveProps(props) {
    console.log(props);
  }

  handleClick = (item) => {
    this.props.navigation.navigate('ContactDetail', {
      contactId: item.id
    })
  }

  createContact = () => {
    this.props.navigation.navigate('ContactDetail', {
      contactId: null
    })
  }

  deleteContact = (item) => {
    console.log('delete', item);
    Alert.alert(
      'Delete contact',
      'Are you sure to delete this contact?',
      [
        // {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
        {
          text: 'Cancel',
          onPress: () => {},
          // style: 'cancel',
        },
        {text: 'OK', onPress: () => {
          this.props.deleteContact(item.id);
        }},
      ],
      {cancelable: false},
    );
  }

  render() {
    return (
      <>
      <TouchableOpacity style={styles.addContact} onPress={this.createContact}>
        <Icon
          size={24}
          name='add' />
        <Text style={styles.textAdd}>Create contact</Text>
      </TouchableOpacity>
      <ScrollView>
        { 
          this.props.contacts.items.map((l, i) => (
            <ListItem
              bottomDivider
              key={i}
              leftAvatar={{ source: { uri: l.avatarUrl } }}
              title={l.name}
              subtitle={l.subtitle}
              onPress={() => this.handleClick(l)}
              rightTitle={<Button type='clear' onPress={() => this.deleteContact(l)} icon={
                <Icon name='delete' color='grey'></Icon>
              } />}
            />
          ))
        }
      </ScrollView>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      contacts: state.contacts
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
      getListContact: () => dispatch(getListContact()),
      deleteContact: (contactId) => dispatch(deleteContact(contactId))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactList);