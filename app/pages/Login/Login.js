import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View,
  Alert,
  ImageBackground,
  AsyncStorage,
  StatusBar,
  SafeAreaView
} from "react-native";
import { Formik } from "formik";
import * as yup from "yup";
import { Text, ButtonGroup, Input, Button, Badge, Icon } from "react-native-elements";
import { StackActions, NavigationActions } from 'react-navigation';
import firebase from 'react-native-firebase';
import sqlite from 'react-native-sqlite-storage';
import { createConnection, getRepository } from 'typeorm'
import Todo from '../../Data/Models/Todo';

const db = sqlite.openDatabase({
  name: 'sqlliteDB.db',
  createFromLocation: '~sqlliteDB.db'
})
class Login extends Component {

  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    
  }

  connect = () => {
    return createConnection({
      type: 'react-native',
      database: 'sqlliteDB.db',
      location: 'default',
      logging: ['error', 'query', 'schema'],
      synchronize: true,
      entities: [
        Todo
      ]
    });
  }

  async demo() {
    await this.connect();
    // await createConnection({
    //   type: 'react-native',
    //   database: 'sqlliteDB.db',
    //   location: 'default',
    //   logging: ['error', 'query', 'schema'],
    //   synchronize: true,
    //   entities: [
    //     Todo
    //   ]
    // });
    const todoRepository = getRepository(Todo);
    const loadedPost = await todoRepository.find();
    console.log(loadedPost);
    // const todo = new Todo({
    //   title: 'new todo',
    //   subtitle: 'sub',
    //   status: 'complete'
    // });
    // const save = await todoRepository.save(todo);
    // console.log(save);
    
  }

  state = {
    selectedIndex: 0,
    userName: "",
    password: "",
    errorMessage: '',
    loading: false
  };

  handleChange() { }

  updateIndex = index => {
    this.setState({ selectedIndex: index });
  };

  handleLogin = (values) => {
    const {email, password} = values;
    this.setState({loading: true});
    firebase.auth().signInWithEmailAndPassword(email, password)
    .then((user) => {
      this.setState({loading: false});
      AsyncStorage.setItem('userToken', user.user.uid);
      this.props.navigation.navigate('App');
    })
    .catch((error) => {
      this.setState({loading: false});
      const { code, message } = error;
      this.setState({errorMessage: message});
    });
  }

  componentDidMount() {
    this.demo();
    // const todos = new Todo();
    // console.log(todos.find();
    
    // db.executeSql('select * from Todo', [], (results) => {
    //   console.log(results.rows.item(0));
    // });
    this.authSubscription = firebase.auth().onAuthStateChanged((user) => {
      this.setState({
        loading: false,
        user,
      });
    });
  }

  componentWillUnmount() {
    this.authSubscription();
  }

  setFieldTouched() { }

  LoginBtn = () => {
    return <Text style={{fontSize: 20}}>Login</Text>;
  };

  SignUpBtn = () => {
    return <Text style={{fontSize: 20}}>Sigup</Text>;
  };

  LoginSection = () => {
    return (
      <Formik
        initialValues={{ email: '', password: '' }}
        onSubmit={values => this.handleLogin(values)}
        validationSchema={yup.object().shape({
          email: yup
            .string()
            .email()
            .required(),
          password: yup
            .string()
            .min(6)
            .required(),
        })}
      >
       {
         ({ values, handleChange, errors, setFieldTouched, touched, isValid, handleSubmit }) => (
          <View style={{
            flex: 1,
            position:'relative'
          }}>
          <View
            style={{
              display: 'flex',
              flexDirection: "column",
              backgroundColor: 'white',
              margin: 15,
              borderRadius: 10
            }}
          >
            <Input
              value={values.email}
              placeholder="Email"
              onChangeText={handleChange('email')}
              onBlur={() => setFieldTouched('email')}
              errorMessage={touched.email && errors.email ? errors.email: ''}
              errorStyle={{ fontSize: 15 }}
            />
            <Input
              placeholder="Password"
              secureTextEntry
              onChangeText={handleChange('password')}
              errorMessage={touched.password && errors.password ? errors.password: ''}
              errorStyle={{ fontSize: 15 }}
            />
            <Button 
              buttonStyle={styles.btnLogin} 
              title="LOGIN"
              onPress={handleSubmit}
              loading={this.state.loading}
            />
            <Text style={styles.errorMessage}>
              {this.state.errorMessage}
            </Text>
          </View>
          <View style={{
            position: 'absolute',
            zIndex: 1
          }}>
            <Text style={{
              width: 50,
              top: 5,
              left: 65,
              backgroundColor: 'white',
              borderRadius: 20
            }}></Text>
          </View>
          </View>
         )
       }
        
      </Formik>
    );
  };

  SignUpSection = () => {
    return (
      <Formik
        initialValues={{ email: '', password: '' }}
        onSubmit={values => this.handleLogin(values)}
        validationSchema={yup.object().shape({
          email: yup
            .string()
            .email()
            .required(),
          password: yup
            .string()
            .min(6)
            .required(),
        })}
      >
       {
         ({ values, handleChange, errors, setFieldTouched, touched, isValid, handleSubmit }) => (
          <View style={{
            flex: 1,
            position:'relative'
          }}>
          <View
            style={{
              display: 'flex',
              flexDirection: "column",
              backgroundColor: 'white',
              margin: 15,
              borderRadius: 10
            }}
          >
            <Input
              value={values.email}
              placeholder="Email"
              onChangeText={() => {}}
              onBlur={() => setFieldTouched('')}
              errorMessage={touched.email && errors.email ? errors.email: ''}
              errorStyle={{ fontSize: 15 }}
            />
            <Input
              placeholder="Password"
              secureTextEntry
              onChangeText={() => {}}
              errorMessage={touched.password && errors.password ? errors.password: ''}
              errorStyle={{ fontSize: 15 }}
            />
            <Input
              placeholder="Confirm password"
              secureTextEntry
              onChangeText={() => {}}
              errorMessage={touched.password && errors.password ? errors.password: ''}
              errorStyle={{ fontSize: 15 }}
            />
            <Button 
              buttonStyle={styles.btnLogin} 
              title="SIGN UP"
              onPress={() => {}}
            />
            <Text style={styles.errorMessage}>
              {this.state.errorMessage}
            </Text>
          </View>
          <View style={{
            position: 'absolute',
            zIndex: 1
          }}>
            <Text style={{
              width: 50,
              top: 5,
              left: 300,
              backgroundColor: 'white',
              borderRadius: 20
            }}></Text>
     
          </View>
          </View>
         )
       }
        
      </Formik>
    );
  };

  render() {
    const buttons = [{ element: this.LoginBtn }, { element: this.SignUpBtn }];
    return (
      // <SafeAreaView style={{flex: 1, backgroundColor: 'transparent'}}>
      <ImageBackground
        source={require("../../../assets/bg-2.png")}
        style={styles.backgroundImage}
      >
        <View style={styles.container}>
          <Text style={styles.welcome}>My contacts</Text>
          <ButtonGroup
            selectedIndex={this.state.selectedIndex}
            onPress={this.updateIndex}
            buttons={buttons}
            selectedButtonStyle={{
              backgroundColor: 'rgb(226,232,228)'
            }}
            buttonStyle={{
              backgroundColor: 'rgb(226,232,228)',
            }}
           
          />
          {this.state.selectedIndex === 0
              ? this.LoginSection()
              : this.SignUpSection()}
        </View>
      </ImageBackground>
      // </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: .7,
    // backgroundColor: 'grey'
  },
  welcome: {
    fontSize: 40,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  backgroundImage: {
    flex: 1,
    resizeMode: "stretch",
    justifyContent: "center",
  },
  btnLogin: {
    marginHorizontal: 10,
    marginTop: 15,
    width: 200,
    alignSelf: 'center',
    backgroundColor: 'rgb(139, 34, 66)',
  },
  errorMessage: {
    color: 'red',
    fontSize: 18,
    marginHorizontal: 10
  }
});

export default Login;