    
import {Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable, ManyToOne, PrimaryColumn} from 'typeorm/browser';


@Entity('Todo')
export default class Todo {

  // constructor(todo: { title: string; subtitle: string, status: string}) {
  //   this.title = todo.title;
  //   this.subtitle = todo.subtitle;
  //   this.status = todo.status;
  // }

  @PrimaryColumn()
  id!: number;

  @Column()
  title!: string;

  @Column("text")
  subtitle!: string;

  @Column("text")
  status!: string;

}
