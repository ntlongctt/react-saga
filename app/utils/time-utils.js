import moment from 'moment';

export const convertToDate = (value) => {
  return value ? moment(value).format('L'): '';
}