
import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import allReducer from './reducer/index';
import rootSaga from './saga/rootSaga';
import logger from 'redux-logger';

const sagaMiddleware = createSagaMiddleware();

const composeEnhancers =
  typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE_ && _DEV__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
    : compose;

export default () => {
  const middlewares = [sagaMiddleware];
  if (__DEV__) {
    middlewares.push(logger);
  }
  const enhancer = composeEnhancers(applyMiddleware(...middlewares));
  const store = createStore(allReducer, enhancer);
  sagaMiddleware.run(rootSaga);
  return store;
};

// export default () => {
//   const midderwares = []
//   const store = applyMiddleware(sagaMiddleware)(createStore)(allReducer);
//   sagaMiddleware.run(rootSaga);
//   return store;
// };
