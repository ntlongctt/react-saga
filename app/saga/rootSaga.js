import { all } from 'redux-saga/effects';

// import { sayHello } from './counterSagas';
import { getContactList } from '../pages/contacts/saga';
import Contacts from '../pages/contacts/saga';

export default function* rootSaga() {
    yield all([
      Contacts(),
    ]);
}