import { combineReducers } from 'redux';
import contactReducer from '../pages/contacts/reducer';

const allReducers = combineReducers({
  contacts:contactReducer,
});

export default allReducers;