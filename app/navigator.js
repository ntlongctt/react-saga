import React, { Component } from 'react';
import {NavigationActions} from 'react-navigation';
import { Avatar } from 'react-native-elements';
import { View, Text} from 'react-native'

// Navigators
import { createSwitchNavigator, createDrawerNavigator, createStackNavigator } from 'react-navigation'

import AuthLoadingScreen from './pages/Auth/AuthLoadingScreen';

import Login from './pages/Login';

import Todo from './pages/Todo/Todo';

import ContactList from './pages/contacts/ContactList';
import ContactDetail from './pages/contacts/ContactDetail';

export const AuthStack = createStackNavigator(
  {
    Login: { screen: Login }
  },
  {
    initialRouteName: "Login"
  }
);

export const Contacts = createStackNavigator(
  {
    ContactList: { screen: ContactList },
    ContactDetail: { screen: ContactDetail }
  },
  {
    initialRouteName: "ContactList"
  }
);

export const Todos = createStackNavigator(
  {
    Todo: { screen: Todo }
  },
  {
    initialRouteName: "Todo"
  }
);

class DrawerContentComponents extends Component {
  navigateToScreen = route => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  };

  render() {
    return (
      <View>
        <View>
          <Avatar
            rounded
            source={{
              uri:
                "https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg"
            }}
          />
        </View>
        <View>
          <View>
            <Text onPress={this.navigateToScreen("Contacts")}>Contacts</Text>
          </View>
          <View>
            <Text onPress={this.navigateToScreen("Todos")}>Todos</Text>
          </View>
        </View>
      </View>
    );
  }
}

export const Drawer = createDrawerNavigator(
  {
    Contacts: { screen: Contacts },
    Todos: { screen: Todos }
  },
  {
    contentComponent: DrawerContentComponents
  }
);

export default AppNavigator = createSwitchNavigator({
  AuthLoading: AuthLoadingScreen,
  App: Drawer,
  Auth: AuthStack
});

