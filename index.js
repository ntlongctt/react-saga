/**
 * @format
 */
import React, {Component} from 'react';
import {AppRegistry} from 'react-native';
// import App from './App';
import {name as appName} from './app.json';
import ContactList from './app/pages/contacts/ContactList';
import ContactDetail from './app/pages/contacts/ContactDetail';
import Login from './app/pages/Login/Login';
import Todo from './app/pages/Todo/Todo';
import AuthLoadingScreen from './app/pages/Auth/AuthLoadingScreen';
import { createAppContainer, createStackNavigator, createSwitchNavigator, createDrawerNavigator } from 'react-navigation';
import allReducer from './app/reducer';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './app/saga/rootSaga';
import configureStore from './app/config';
import AppNavigator from './app/navigator';
import 'reflect-metadata';

// const sagaMiddleware = createSagaMiddleware();
// let store = createStore(allReducer, applyMiddleware(sagaMiddleware));


// const ContactsStack = createStackNavigator({
//   Contacts: ContactList,
//   ContactDetail: ContactDetail
// })

// const AuthStack = createStackNavigator({Login: Login});

// const AppNavigator = createSwitchNavigator({
//   AuthLoading: AuthLoadingScreen,
//   App: AppDrawer,
//   Auth: AuthStack
// });

// const AppDrawer = createDrawerNavigator({
//   Todo: {
//     screen: Todo,
//   },
//   Contacts: {
//     screen: ContactsStack,
//   },
// });

let Navigation = createAppContainer(AppNavigator);

const App = () => (
  <Provider store={configureStore()}>
      <Navigation />
  </Provider>
);
// sagaMiddleware.run(rootSaga);
AppRegistry.registerComponent(appName, () => App);


