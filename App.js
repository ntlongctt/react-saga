/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component, Fragment } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Alert,
  ImageBackground
} from "react-native";
import { Formik } from "formik";
import * as yup from "yup";
import { Text, ButtonGroup, Input, Button } from "react-native-elements";
import { createAppContainer, createStackNavigator, StackActions, NavigationActions, createSwitchNavigator } from 'react-navigation';

import ContactList from './app/pages/contacts/ContactList';
import Login from './app/pages/Login/Login';
import AuthLoadingScreen from './app/pages/Auth/AuthLoadingScreen';
import allReducer from './app/reducer';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './app/saga/rootSaga';
// const sagaMiddleware = createSagaMiddleware();


// let store = createStore(allReducer, applyMiddleware(sagaMiddleware));
const AppStack = createStackNavigator({Contacts: ContactList})

const AuthStack = createStackNavigator({Login: Login})

const AppNavigator = createSwitchNavigator( { AuthLoading: AuthLoadingScreen, App: AppStack, Auth: AuthStack, } );

let Navigation = createAppContainer(AppNavigator);

// export default createAppContainer(AppNavigator);
export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Navigation></Navigation>
      </Provider>
    );
  }
}

